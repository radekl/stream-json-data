"""
script to connect to localhost TCP port 3291 and listen for JSON
file.

It outputs data to two files:
 - first file will append values in new line in format:
    alpha VALUE, bravo VALUE, delta VALUE
 - second file keep only last values for the keys

If the connection is closed the script will exit with message.
We could possibly try to reconnect until server is up again.

There is now specification on server if data can be missing some key: value.
If this cna happen we have to make sure the script handle this situations.
"""

import socket
import json

HOST, PORT = "localhost", 3291
APPENDED_DATA_FILENAME = 'data_appended.txt'
LAST_VALUE_FILENAME = 'data_last_value.txt'

try:
    while True:
        # Create a socket (SOCK_STREAM means a TCP socket)
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
            # Connect to server and send data
            sock.connect((HOST, PORT))
            # Receive data from the server and shut down
            received = json.loads(sock.recv(1024))
            # assume the received data has always all keywords and values
            if received:
                assert 'alpha' in received.keys()
                assert 'bravo' in received.keys()
                assert 'delta' in received.keys()

                with open(APPENDED_DATA_FILENAME, 'a') as f:
                    f.write(f"alpha {received['alpha']}, "
                            f"bravo {received['bravo']}, "
                            f"delta {received['delta']} \n")
                with open(LAST_VALUE_FILENAME, 'w') as f:
                    f.write(f"alpha {received['alpha']}"
                            f"\nbravo {received['bravo']}"
                            f"\ndelta {received['delta']}")

except ConnectionError as e:
    # we could possibly try to reconnect and wait until server is up again
    exit(e)
