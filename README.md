# Stream client

### Requirements
write a function/class in Python3 doing the following:
read network stream (in JSON) on port TCP 3291
write to local file keys "alpha", "bravo", "charlie" and their corresponding values.

# How to run
NOTE required Python3.7 and higher

Open terminal and run server:
```
python server.py
```

Open another terminal and run client:
```
python client.py
```

The client will create two files:
  - data_appended.txt where each new line in format:
    alpha VALUE, bravo VALUE, delta VALUE
    are received data
  - data_last_value.txt keeps only last values for the keys


If the connection is closed the script will exit with message.

TODO: We could possibly try to reconnect until server is up again.

There is now specification on server if data can be missing some key: value.

TODO: make sure the script handle the situation where data can be incomplet.
