"""
Modifed version based on example:
https://docs.python.org/3/library/socketserver.html?highlight=socket#module-socketserver

"""

import socketserver
import json
import random


class MyTCPHandler(socketserver.BaseRequestHandler):
    """
    The request handler class for our server.

    It is instantiated once per connection to the server, and must
    override the handle() method to implement communication to the
    client.
    """

    def handle(self):
        # self.request is the TCP socket connected to the client
        self.data = json.dumps({
            'alpha': random.random(),
            'bravo': random.random(),
            'delta': random.random()})
        print(f"acceptet connection from {self.client_address[0]}")
        self.request.sendall(bytes(self.data, encoding="utf-8"))


if __name__ == "__main__":
    HOST, PORT = "localhost", 3291

    # Create the server, binding to localhost on port 3291
    with socketserver.TCPServer((HOST, PORT), MyTCPHandler) as server:
        # Activate the server; this will keep running until you
        # interrupt the program with Ctrl-C
        server.serve_forever()
